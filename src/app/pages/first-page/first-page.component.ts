import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import * as D3 from "d3";

@Component({
  selector: "app-first-page",
  templateUrl: "./first-page.component.html",
  styleUrls: ["./first-page.component.scss"],
})
export class FirstPageComponent implements OnInit {
  @ViewChild("graphContainer", { read: ElementRef, static: false })
  graphContainer: ElementRef;

  radius = 20;
  noOfItems = 1000;
  constructor() {}

  ngOnInit(): void {
    setTimeout(() => this.renderShit());
  }

  renderShit() {
    const elHeight = this.graphContainer.nativeElement.offsetHeight * 0.5;
    const elWidth = this.graphContainer.nativeElement.offsetWidth;
    const svg = D3.select(this.graphContainer.nativeElement)
      .append("svg")
      .attr("width", elWidth)
      .attr("height", elHeight);

    //data to bind --500 items
    const circles = D3.range(this.noOfItems).map((i) => ({
      x: Math.random() * (elWidth - this.radius * 2) + this.radius,
      y: Math.random() * (elHeight - this.radius * 2) + this.radius,
    }));

    svg
      .selectAll("circle")
      .data(circles)
      .join("circle")
      .attr("cx", (d) => d.x)
      .attr("cy", (d) => d.y)
      .attr("r", this.radius)
      .attr("fill", (d, i) => D3.schemeCategory10[i % 10]);

    let drag_handler = D3.drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended);

    function dragstarted() {
      D3.select(this).attr("stroke", "black");
    }

    function dragged(event, d) {
      D3.select(this)
        .raise()
        .attr("cx", (d.x = event.x))
        .attr("cy", (d.y = event.y));
    }

    function dragended() {
      D3.select(this).attr("stroke", null);
    }

    drag_handler(D3.selectAll("circle"));
  }
}
