import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import * as D3 from "d3";
import { MyTree, treeData } from "./tree-data";

@Component({
  selector: "app-fourth-page",
  templateUrl: "./fourth-page.component.html",
  styleUrls: ["./fourth-page.component.scss"],
})
export class FourthPageComponent implements OnInit {
  @ViewChild("graphContainer", { read: ElementRef, static: false })
  graphContainer: ElementRef;
  numberOfNodes = 5;
  constructor() {}

  tree = D3.tree;
  hierarchy = D3.hierarchy;
  select = D3.select;

  ngOnInit(): void {
    // setTimeout(() => this.renderBar());
    setTimeout(() => this.renderTree());
  }

  renderBar() {
    var homeless = [
      { state: "California", population: 129972 },
      { state: "New York", population: 91897 },
      { state: "Florida", population: 31030 },
      { state: "Texas", population: 25310 },
      { state: "Washington", population: 22304 },
    ];

    var bar = D3.select(this.graphContainer.nativeElement)
      .append("svg")
      .selectAll()
      .data(homeless);

    bar
      .join("rect")
      .attr("height", 19)
      .attr("width", (d) => d.population / 1000)
      .attr("x", 100)
      .attr("y", (d, i) => i * 20)
      .attr("fill", "pink");

    bar
      .join("text")
      .text((d) => d.state)
      .attr("x", 106)
      .attr("text-anchor", "start")
      .attr("y", (d, i) => i * 19 + 17);
    // .attr('y', (d, i) => i * 19);
  }

  renderTree() {

    let myTree = new MyTree();
    myTree.$onInit(this.graphContainer.nativeElement);
  }


}
