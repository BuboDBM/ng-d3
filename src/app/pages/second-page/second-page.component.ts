import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import * as D3 from "d3";
import { Grid } from "src/app/grid";
import { params } from "src/app/params";

@Component({
  selector: "app-second-page",
  templateUrl: "./second-page.component.html",
  styleUrls: ["./second-page.component.scss"],
})
export class SecondPageComponent implements OnInit {
  @ViewChild("graphContainer", { read: ElementRef, static: false })
  graphContainer: ElementRef;

  width = 400;
  height = 400;
  radius = 20;
  numbOfCirc = 100;
  constructor() {}

  ngOnInit(): void {
    setTimeout(() => this.renderCircles());
  }

  renderCircles() {
    params.height = this.graphContainer.nativeElement.offsetHeight * 0.95;
    params.width = this.graphContainer.nativeElement.offsetWidth;
    const cWidth = this.width;
    const cHeight = this.height;
    const svg = D3.select(this.graphContainer.nativeElement)
      .append("svg")
      .append("g")
      .attr(
        "transform",
        `translate(${params.width / 2}, ${params.height / 2})`
      );

    const grid = new Grid(svg);
    var radius = 20;

    var circle_data = D3.range(this.numbOfCirc).map(function () {
      return {
        x: Math.round(
          Math.random() * (params.width - radius * 2) +
            radius -
            params.width * 0.5
        ),
        y: Math.round(
          Math.random() * (params.height - radius * 2) +
            radius -
            params.height * 0.5
        ),
      };
    });

    const g = svg.append("g").attr("id", "mamojeb").attr("cursor", "grab");

    g.append("g")
      .attr("class", "circles")
      .selectAll("circle")
      .data(circle_data)
      .enter()
      .append("circle")
      .attr("cx", function (d) {
        return d.x;
      })
      .attr("cy", function (d) {
        return d.y;
      })
      .attr("r", radius)
      .attr("fill", (d, i) => D3.interpolateRainbow(i / 360))
      .call(
        D3.drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended)
      );

      svg.call(D3.zoom()
      .extent([[0, 0], [params.width, params.height]])
      .scaleExtent([0.5, 5])
      .on("zoom", zoomed));

    // let drag_handler = D3.drag()
    //   .on("start", dragstarted)
    //   .on("drag", dragged)
    //   .on("end", dragended);

    // let zoom_handler = D3.zoom()
    // .on("zoom", zoom_actions);

    function zoomed({ transform }) {
      g.attr("transform", transform);
    }

    function dragstarted() {
      D3.select(this).attr("stroke", "black");
    }

    function dragged(event, d) {
      D3.select(this)
        .raise()
        .attr("cx", (d.x = event.x))
        .attr("cy", (d.y = event.y));
    }

    function dragended() {
      D3.select(this).attr("stroke", null);
    }

    // drag_handler(D3.selectAll("circle"));
  }
}
