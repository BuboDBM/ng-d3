import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as D3 from "d3";
import { Grid } from "src/app/grid";
import { params } from "src/app/params";

@Component({
  selector: 'app-third-page',
  templateUrl: './third-page.component.html',
  styleUrls: ['./third-page.component.scss']
})
export class ThirdPageComponent implements OnInit {
  @ViewChild("graphContainer", { read: ElementRef, static: false })
  graphContainer: ElementRef;

  width = 400;
  height = 400;
  radius = 20;

  numberOfNodes = 50;

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => this.renderCircles());
  }

  renderCircles() {
    params.height = this.graphContainer.nativeElement.offsetHeight * 0.95;
    params.width = this.graphContainer.nativeElement.offsetWidth;

    const svg = D3.select(this.graphContainer.nativeElement)
      .append("svg")
      .append("g")
      .attr(
        "transform",
        `translate(${params.width / 2}, ${params.height / 2})`
      );

    const radius = 20;

    let circle_data = [];
    for (let i = 0; i < this.numberOfNodes; i++) {
      circle_data.push(
        {
          id: i,
          x: Math.round(
            Math.random() * (params.width - radius * 2) +
              radius -
              params.width * 0.5
          ),
          y: Math.round(
            Math.random() * (params.height - radius * 2) +
              radius -
              params.height * 0.5
          ),
        }
      );
    }

    let links = [];
    for (let i = 0; i < circle_data.length-1; i++) {
      links.push(
        D3.linkVertical()({
          source: [circle_data[i].x, circle_data[i].y],
          target: [circle_data[i+1].x, circle_data[i+1].y]
        })
      );
    }

    for (let i = 0; i < links.length; i++) {
      svg
        .append('path')
        .attr('d', links[i])
        .attr('stroke', 'black')
        .attr('fill', 'none');
    }

    const g = svg.append("g").attr("id", "circ");//.attr("cursor", "grab");

    g.append("g")
      .attr("class", "circles")
      .selectAll("circle")
      .data(circle_data)
      .enter()
      .append("circle")
      .attr("cx", function (d) {
        return d.x;
      })
      .attr("cy", function (d) {
        return d.y;
      })
      .attr("r", radius)
      .attr("fill", "orange")
      .attr("stroke", "orangered")
      // .call(
      //   D3.drag()
      //     .on("start", dragstarted)
      //     .on("drag", dragged)
      //     .on("end", dragended)
      // );

      // svg.call(D3.zoom()
      // .extent([[0, 0], [params.width, params.height]])
      // .scaleExtent([0.5, 5])
      // .on("zoom", zoomed));

    function zoomed({ transform }) {
      g.attr("transform", transform);
    }

    function dragstarted() {
      D3.select(this).attr("stroke", "black");
    }

    function dragged(event, d) {
      D3.select(this)
        .raise()
        .attr("cx", (d.x = event.x))
        .attr("cy", (d.y = event.y));
    }

    function dragended() {
      D3.select(this).attr("stroke", null);
    }

    // drag_handler(D3.selectAll("circle"));
  }

}
