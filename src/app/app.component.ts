import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import * as D3 from "d3";
import { Grid } from "./grid";
import { params } from "./params";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {

  // @ViewChild("graphContainer", { read: ElementRef, static: false }) graphContainer: ElementRef;

  // width = 400;
  // height = 400;
  // /** SVG width */
  // // width:number = 0;
  // // /** SVG height */
  // // height:number = 0;
  // timer;
  // radius = 20;

  // ngOnInit() {
  //   //setTimeout(() => this.renderShit());
  // }

  // renderShit() {
  //   const elHeight = this.graphContainer.nativeElement.offsetHeight;
  //   const elWidth = this.graphContainer.nativeElement.offsetWidth;
  //   const svg = D3.select(this.graphContainer.nativeElement)
  //     .append("svg")
  //     .attr("width", elWidth)
  //     .attr("height", elHeight);

  //     //data to bind
  //   const circles = D3.range(500).map((i) => ({
  //     x: Math.random() * (this.width - this.radius * 2) + this.radius,
  //     y: Math.random() * (this.height - this.radius * 2) + this.radius,
  //   }));

  //   svg
  //     .selectAll("circle")
  //     .data(circles)
  //     .join("rect")
  //     // .attr("cx", (d, i) => d.x + i)
  //     // .attr("cy", (d, i) => d.y + i)
  //     // .attr("r", this.radius)
  //     .attr("x", (d) => d.x)
  //     .attr("y", (d) => d.y)
  //     .attr("width", 100)
  //     .attr("height", 40)
  //     .attr("fill", (d, i) => D3.schemeCategory10[i % 10]);

  //   let drag_handler = D3.drag()
  //     .on("start", dragstarted)
  //     .on("drag", dragged)
  //     .on("end", dragended);

  //   function dragstarted() {
  //     D3.select(this).attr("stroke", "black");
  //   }

  //   function dragged(event, d) {
  //     D3.select(this)
  //       .raise()
  //       .attr("cx", (d.x = event.x))
  //       .attr("cy", (d.y = event.y));
  //   }

  //   function dragended() {
  //     D3.select(this).attr("stroke", null);
  //   }

  //   drag_handler(D3.selectAll("circle"));
  // }

  // private renderCircles() {
  //   const cWidth = this.width;
  //   const cHeight = this.height;
  //   let svg = D3.select(this.graphContainer.nativeElement)
  //     .append("svg")
  //     .append("g")
  //     .attr(
  //       "transform",
  //       `translate(${params.width / 2}, ${params.height / 2})`
  //     );

  //   let grid = new Grid(svg);

  //   //used to capture drag position
  //   var start_x, start_y;

  //   //create some circles at random points on the screen
  //   //create 50 circles of radius 20
  //   //specify centre points randomly through the map function
  //   var radius = 20;

  //   var circle_data = D3.range(50).map(function () {
  //     return {
  //       x: Math.round(
  //         Math.random() * (cWidth - radius * 2) + radius - cWidth * 0.5
  //       ),
  //       y: Math.round(
  //         Math.random() * (cHeight - radius * 2) + radius - cHeight * 0.5
  //       ),
  //     };
  //   });

  //   //funky yellow circles
  //   var circles = svg
  //     .append("g")
  //     .attr("class", "circles")
  //     .selectAll("circle")
  //     .data(circle_data)
  //     .enter()
  //     .append("circle")
  //     .attr("cx", function (d) {
  //       return d.x;
  //     })
  //     .attr("cy", function (d) {
  //       return d.y;
  //     })
  //     .attr("r", radius)
  //     .attr("fill", "red");
  // }

  update(elapsed: number) {}

  // private renderGraph() {
  //   const svg = D3.select(this.graphContainer.nativeElement)
  //     .append("svg")
  //     .attr("width", this.width)
  //     .attr("height", this.height)
  //     .attr("class", "bar-chart")
  //     .append("g");

  //   svg
  //     .selectAll("rect")
  //     .data(this.dataset)
  //     .enter()
  //     .append("rect")
  //     .attr("x", (d, i) => {
  //       return i * 20;
  //     })
  //     .attr("width", this.width / this.dataset.length - 1)
  //     .attr("y", function (d) {
  //       return 200 - d * 5;
  //     })
  //     .attr("height", function (d) {
  //       return d * 5;
  //     })
  //     .attr("fill", "teal");

  //   svg
  //     .selectAll("text")
  //     .data(this.dataset)
  //     .enter()
  //     .append("text")
  //     .text((d) => d)
  //     .attr("x", (d, i) => i * (this.width / this.dataset.length) + 4)
  //     .attr("y", (d) => this.height - d * 5 + 12)
  //     .attr("font-family", "sans-serif")
  //     .attr("font-size", "11px")
  //     .attr("fill", "white");
  // }
}
