import * as D3 from "d3";

// // Angular stuff
// import { Component, OnInit, OnDestroy } from "@angular/core";
// import { params } from "./params";
// @Component({ selector: "null", template: `<svg></svg>` })
// export class GridPattern implements OnInit {
//   constructor() {}
//   ngOnInit() {
//     let svg = D3.select("svg")
//       .attr("width", params.width)
//       .attr("height", params.height);

//     new Grid(svg);
//   }
// }

// Pattern reference: https://stackoverflow.com/a/14209704/2496170
export const GridDef:string = `
<pattern id="smallGrid" width="8" height="8" patternUnits="userSpaceOnUse">
    <path d="M 8 0 L 0 0 0 8" fill="none" stroke="gray" stroke-width="0.35"/>
</pattern>
`;

/**
 * SVG Grid
 *
 */
export class Grid {
  svg;
  /** Grid selector ( group 'g' ) */
  grid;

  x: number = 0;
  y: number = 0;

  constructor(svg) {
    this.svg = svg;

    svg.append("defs").html(GridDef);

    this.x = svg.attr("width") / 2;
    this.y = svg.attr("height") / 2;

    this.grid = svg
      .append("g")
      .attr("transform", `translate(${this.x}, ${this.y})`);

    this.grid
      .append("rect") // grid pattern rect
      .attr("fill", "url(#smallGrid)")
      .attr("x", "-50%")
      .attr("y", "-50%")
      .attr("width", "100%")
      .attr("height", "100%");
  }
}
