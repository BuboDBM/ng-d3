export namespace params {


  /** scalar value origin coordinates relative to window, this variable
  will affect how the stageMouse() is calculating the coordinates
  eg. origin=[0.5,0.5] is the center of the screen, mouse at top left will be -width/2, -height/2 */
  export var origin:number[] = [ 0, 0 ];

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // The following variables are being updated within app/app.component.t

  /** Mouse horizontal position relative to svg origin */
  export var mouseX:number = 0;
  /** Mouse vertical position relative to svg origin */
  export var mouseY:number = 0;
  /** SVG width */
  export var width:number = 0;
  /** SVG height */
  export var height:number = 0;
}
